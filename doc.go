/*
# conv

#### 介绍
基本 `go` 语言快速类型转换方法

#### 使用说明

##### 快速转换

1. `DefaultString` 快速转换对象到 string，当失败时返回默认值

   ```go
   // DefaultString 快速转换对象到 string，当失败时返回默认值
   //	@v 需要转换的对象
   //	@defaultValue 传参第一个值为默认值，不设置则默认为空字符串
   //	returns
   //		@string string
   func DefaultString(v interface{}, defaultValue ...string) string
   ```



2. `DefaultInt` 快速转换对象到 int，当失败时返回默认值

   ```go
   // DefaultInt 快速转换对象到 int，当失败时返回默认值
   //	@v 需要转换的对象
   //	@defaultValue 传参第一个值为默认值，不设置则默认为 0
   //	returns
   //		@int int
   func DefaultInt(v interface{}, defaultValue ...int) int
   ```



3. `DefaultInt64` 快速转换对象到 int64，当失败时返回默认值

   ```go
   // DefaultInt64 快速转换对象到 int64，当失败时返回默认值
   //	@v 需要转换的对象
   //	@defaultValue 传参第一个值为默认值，不设置则默认为 0
   //	returns
   //		@int int
   func DefaultInt64(v interface{}, defaultValue ...int64) int64
   ```



4. `DefaultFloat` 快速转换对象到 float32，当失败时返回默认值

   ```go
   // DefaultFloat 快速转换对象到 float32，当失败时返回默认值
   //	@v 需要转换的对象
   //	@defaultValue 传参第一个值为默认值，不设置则默认为 0
   //	returns
   //		@float32 float32
   func DefaultFloat(v interface{}, defaultValue ...float32) float32
   ```



5. `DefaultFloat64` 快速转换对象到 float64，当失败时返回默认值

   ```go
   // DefaultFloat64 快速转换对象到 float64，当失败时返回默认值
   //	@v 需要转换的对象
   //	@defaultValue 传参第一个值为默认值，不设置则默认为 0
   //	returns
   //		@float64 float64
   func DefaultFloat64(v interface{}, defaultValue ...float64) float64
   ```








##### 类型转换

1. `AsBytes` 将反射值类型转换为字节流

   ```go
   // AsBytes 将反射值转换为原始字节流
   //	@buf 写入的目标缓冲流
   //	@rv 需要写入的反射值
   //	returns
   //		@b 写入完成的目标缓冲流
   //		@ok 是否写入成功
   AsBytes(buf []byte, rv reflect.Value) (b []byte, ok bool)
   ```



2. `AsString` 将 interface 转换为 string

   ```go
   // AsString 将 interface 转换为 string
   //	@src 需要转换的源
   //	returns
   //		@string 转换后的 string
   func AsString(src interface{}) string
   ```



3. `AsFloat64`  将 interface 转换为 float64

   ```go
   // AsFloat64 将 interface 转换为 float64
   //	@src 需要转换的源
   //	returns
   //		@float64 转换后的 float64 值
   //		@error 是否转换成功
   func AsFloat64(src interface{}) (float64, error)
   ```



4. `AsFloat` 将 interface 转换为 float32

   ```go
   // AsFloat 将 interface 转换为 float32
   //	@src 需要转换的源
   //	returns
   //		@float32 转换后的 float32 值
   //		@error 是否转换成功
   func AsFloat(src interface{}) (float32, error)
   ```




5.  `AsInt64 ` 将 interface 转换为 int64

   ```go
   // AsInt64 将 interface 转换为 int64
   //	@src 需要转换的源
   //	returns
   //		@int64 转换后的 int64 值
   //		@error 是否转换成功
   func AsInt64(src interface{}) (int64, error)
   ```




6.  `AsInt ` 将 interface 转换为 int

   ```go
   // AsInt64 将 interface 转换为 int64
   //	@src 需要转换的源
   //	returns
   //		@int64 转换后的 int64 值
   //		@error 是否转换成功
   func AsInt64(src interface{}) (int64, error)
   ```




7. `AsBool` 将字节流转换为 bool

   ```go
   // AsBool 将字节流转换为 bool
   //	@src 需要转换的源
   //	returns
   //		@bool 转换后的 bool 值
   //		@error 是否转换成功
   func AsBool(bs []byte) (bool, error)
   ```




8. `AsMapStringInterface`  将 interface 转换为 map[string]interface{}

   ```go
   // AsMapStringInterface 将 interface 转换为 map[string]interface{}
   //	@src 需要转换的源
   //	returns
   //		@map 转换后的 map[string]interface{}
   //		@error 是否转换成功
   func AsMapStringInterface(src interface{}) (map[string]interface{}, error)
   ```




9. `AsMapInterface`  将 interface 转换为 map[interface{}]interface{}

   ```go
   // AsMapInterface 将 interface 转换为 map[interface{}]interface{}
   //	@src 需要转换的源
   //	returns
   //		@map 转换后的 map[interface{}]interface{}
   //		@error 是否转换成功
   func AsMapInterface(src interface{}) (map[interface{}]interface{}, error)
   ```




10. `MapInterfaceToMapStringInterface`  将 map[interface{}]interface{} 转换为 map[string]interface{}

    ```go
    // MapInterfaceToMapStringInterface 将 map[interface{}]interface{} 转换为 map[string]interface{}
    //	@src 需要转换的源
    // 	@dest 转换的目标
    func MapInterfaceToMapStringInterface(src map[interface{}]interface{}, dest map[string]interface{})
    ```



11. `ConvertAssign` 将 src 中的值转换并拷贝到 dest 中

    ```go
    // ConvertAssign 将 src 中的值转换并拷贝到 dest 中
    // 当转换出现丢失时，返回一个 error
    // dest 为一个 pointer 类型
    //	@src 需要转换的源
    // 	@dest 转换的目标
    //	returns
    //		@error 是否转换成功
    func ConvertAssign(dest, src interface{}) error
    ```



*/
package conv
