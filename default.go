package conv

// DefaultString 快速转换对象到 string，当失败时返回默认值
//	@v 需要转换的对象
//	@defaultValue 传参第一个值为默认值，不设置则默认为空字符串
//	returns
//		@string string
func DefaultString(v interface{}, defaultValue ...string) string {
	dv := ``

	if len(defaultValue) > 0 {
		dv = defaultValue[0]
	}

	asv := AsString(v)
	if asv == `` {
		return dv
	}

	return asv
}

// DefaultInt 快速转换对象到 int，当失败时返回默认值
//	@v 需要转换的对象
//	@defaultValue 传参第一个值为默认值，不设置则默认为 0
//	returns
//		@int int
func DefaultInt(v interface{}, defaultValue ...int) int {
	dv := 0

	if len(defaultValue) > 0 {
		dv = defaultValue[0]
	}

	if asv, err := AsInt(v); err == nil {
		return asv
	}

	return dv
}

// DefaultInt64 快速转换对象到 int64，当失败时返回默认值
//	@v 需要转换的对象
//	@defaultValue 传参第一个值为默认值，不设置则默认为 0
//	returns
//		@int int
func DefaultInt64(v interface{}, defaultValue ...int64) int64 {
	var dv int64 = 0

	if len(defaultValue) > 0 {
		dv = defaultValue[0]
	}

	if asv, err := AsInt64(v); err == nil {
		return asv
	}

	return dv
}

// DefaultFloat 快速转换对象到 float32，当失败时返回默认值
//	@v 需要转换的对象
//	@defaultValue 传参第一个值为默认值，不设置则默认为 0
//	returns
//		@float32 float32
func DefaultFloat(v interface{}, defaultValue ...float32) float32 {
	var dv float32 = 0

	if len(defaultValue) > 0 {
		dv = defaultValue[0]
	}

	if asv, err := AsFloat(v); err == nil {
		return asv
	}

	return dv
}

// DefaultFloat64 快速转换对象到 float64，当失败时返回默认值
//	@v 需要转换的对象
//	@defaultValue 传参第一个值为默认值，不设置则默认为 0
//	returns
//		@float64 float64
func DefaultFloat64(v interface{}, defaultValue ...float64) float64 {
	var dv float64 = 0

	if len(defaultValue) > 0 {
		dv = defaultValue[0]
	}

	if asv, err := AsFloat64(v); err == nil {
		return asv
	}

	return dv
}
